import cv2
from PIL import Image,ImageTk
from yolo import YOLO, detect_video
import numpy as np
from timeit import default_timer as timer
vid = cv2.VideoCapture(0)
if not vid.isOpened():
    raise IOError("Couldn't open webcam or video")
#video_FourCC = int(vid.get(cv2.CAP_PROP_FOURCC))
video_FourCC = cv2.VideoWriter_fourcc(*'mp4v')
video_fps = vid.get(cv2.CAP_PROP_FPS)
video_size = (int(vid.get(cv2.CAP_PROP_FRAME_WIDTH)),int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT)))
yolo = YOLO()
output_path = "C:/Users/ben/Desktop/face-recognition/face-recognition"
isOutput = False
if output_path:
    isOutput = True
if isOutput:
    print("!!! TYPE:", type(output_path), type(video_FourCC), type(video_fps), type(video_size))
    out = cv2.VideoWriter(output_path, video_FourCC, video_fps, video_size)
accum_time = 0
curr_fps = 0
fps = "FPS: ??"
prev_time = timer()
while True:
    return_value, frame = vid.read()
    image = Image.fromarray(frame)
    image = yolo.detect_image(image)
    result = np.asarray(image)
    curr_time = timer()
    exec_time = curr_time - prev_time
    prev_time = curr_time
    accum_time = accum_time + exec_time
    curr_fps = curr_fps + 1
    if accum_time > 1:
        accum_time = accum_time - 1
        fps = "FPS: " + str(curr_fps)
        curr_fps = 0
    cv2.putText(result, text=fps, org=(3, 15), fontFace=cv2.FONT_HERSHEY_SIMPLEX,fontScale=0.50, color=(255, 0, 0), thickness=2)
    cv2.namedWindow("result", cv2.WINDOW_NORMAL)
    cv2.imshow("result", result)
    if isOutput:
        out.write(result)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
out.release()
yolo.close_session()