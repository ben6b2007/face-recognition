# Face Recognition
## Dataset
 
UTKFace

Data Size: 20000

Link:https://susanqq.github.io/UTKFace/

----
## Training tool

keras-yolo3

Link: https://github.com/qqwweee/keras-yolo3

---- 
## Bounding box tool 

labelImg

Link: https://github.com/tzutalin/labelImg

---- 
## Vocdevkit
Storage dataset and annotations

    face-recognition
    |-- keras-yolov3
        |-- Vocdevkit
            |-- VOC2007
                |-- Annotations <-- Bounding box
                |-- ImageSets 
                |-- JPEGImages <-- Storage img
                |-- make_main_txt.py <-- split dataset into train set, test set and val set

## Translate PasalVOC format to yolo format (Bounding box)
To split the dataset, run the voc_annotation.py

## Training
Run VOCdevkit\VOC2007\make_main_txt.py <-- split dataset
Run voc_annotation.py <-- convert the format
Run train.py

The result will show in model_data/logs
